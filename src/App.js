import React, {Component, Fragment} from 'react';
import {Switch, Route} from 'react-router-dom';

import Header from './components/Header/Header';
import AddPost from './containers/AddPost/AddPost';
import HomePage from './containers/HomePage/HomePage';
import FullPost from './containers/FullPost/FullPost';

import './App.css';
import EditPost from "./containers/EditPost/EditPost";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header/>
                <Switch>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/posts" exact component={HomePage} />
                    <Route path="/posts/add" component={AddPost} />
                    <Route path="/posts/:id" exact component={FullPost} />
                    <Route path="/posts/:id/edit" component={EditPost} />
                </Switch>
            </Fragment>
        );
    }
}

export default App;
